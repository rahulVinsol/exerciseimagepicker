//
//  MainViewController.swift
//  ExerciseImagePicker
//
//  Created by Rahul Rawat on 07/06/21.
//

import UIKit
import MobileCoreServices
import AVKit

class MainViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var pickImageButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pickImageButton.layer.cornerRadius = 10
    }
    

    @IBAction func pickButtonClicked(_ sender: UIButton) {
        showImagePickingSheet()
    }
    
}

extension MainViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func showImagePickingSheet(){
        let actionSheet = UIAlertController(title: "Choose Image from", message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { [weak self] action in
            self?.checkAndShowPicker(sourceType: .photoLibrary)
        }))
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { [weak self] action  in
            self?.checkAndShowPicker(sourceType: .camera)
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(actionSheet, animated: true, completion: nil)
    }
    
    func checkAndShowPicker(sourceType: UIImagePickerController.SourceType) {
        if !UIImagePickerController.isSourceTypeAvailable(sourceType) {
            var message: String?
            switch sourceType {
            case .camera:
                message = "Can't Find Camera"
            case .photoLibrary:
                message = "Can't Find Photo Library"
            case .savedPhotosAlbum:
                message = "Can't Find Saved Albums"
            default:
                message = nil
            }

            let alert = UIAlertController(title: "Not Supported", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
            present(alert, animated: true, completion: nil)
            return
        }
        
        if sourceType == .camera && !checkCameraPermission() {
            return
        }
        
        showPicker(sourceType: sourceType)
    }
    
    func checkCameraPermission() -> Bool {
        switch AVCaptureDevice.authorizationStatus(for: .video){
        case .authorized:
            return true
        case .denied:
            showPermissionDeniedVC()
            return false
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { [weak self] success in
                DispatchQueue.main.async {
                    if success {
                        self?.showPicker(sourceType: .camera)
                    } else {
                        self?.showPermissionDeniedVC()
                    }
                }
            })
            return false
        case .restricted:
            return false
        default:
            return false
        }
    }
    
    func showPermissionDeniedVC() {
        let alert = UIAlertController(title: "Permission Denied", message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Settings", style: .default, handler: { action in
            if let url = URL(string: UIApplication.openSettingsURLString) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    private func showPicker(sourceType: UIImagePickerController.SourceType) {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.allowsEditing = true
        imagePickerController.mediaTypes = [kUTTypeMovie as String, kUTTypeImage as String]
        imagePickerController.sourceType = sourceType
        
        present(imagePickerController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        dismiss(animated: true, completion: nil)

        switch info[UIImagePickerController.InfoKey.mediaType] as? String {
        
        case String(kUTTypeImage):
            let pickedImage = (info[UIImagePickerController.InfoKey.editedImage] ?? info[UIImagePickerController.InfoKey.originalImage]) as? UIImage
            if let imageToShow = pickedImage {
                imageView.image = imageToShow
            }
            
        case String(kUTTypeMovie):
            if let mediaUrl = info[UIImagePickerController.InfoKey.mediaURL] as? URL {
                playVideo(url: mediaUrl)
            }
        
        default:
            break
        }
    }
    
    private func playVideo(url: URL) {
        let playerViewController = AVPlayerViewController()
        playerViewController.player = AVPlayer(url: url)
//        playerViewController.player?.play()
        
        present(playerViewController, animated: true, completion: nil)
    }
}
